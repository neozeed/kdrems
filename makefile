CFLAGS = -mm -b -a -K -I. -H -O2
#LDFLAGS = /yx /m /l 
LDFLAGS = /yx 
ASM = TASM
ASMFLAGS = /MX /ZI /O 


OBJS =	id_ca.obj	id_rf.obj	kd_act2.obj	lzhuf.obj \
	kd_play.obj	id_in.obj	id_sd.obj	id_vw.obj \
	kd_demo.obj	id_mm.obj	id_us.obj	kd_act1.obj \
	kd_keen.obj	kd_main.obj	gelib.obj	soft.obj \
	jam_io.obj

AOBJS = id_rf_a.obj	id_us_a.obj	id_vw_a.obj

SOBJS = context.obj	gametext.obj	kdradict.obj	kdrahead.obj \
	kdredict.obj	kdrehead.obj \
	kdrmdict.obj	kdrmhead.obj	story.obj


kdreams.exe:	$(OBJS) $(AOBJS) $(SOBJS)
		tlink $(LDFLAGS) @makersp

#	there is an error building ID_US.OBJ
#	build it with the UI until I figure it out
id_us.obj:
	copy ui\id_us.obj .

id_vw_a.obj:
	$(ASM) $(ASMFLAGS) id_vw_a.asm

id_rf_a.obj:
	$(ASM) $(ASMFLAGS) id_rf_a.asm

id_us_a.obj:
	$(ASM) $(ASMFLAGS) id_us_a.asm

kdradict.obj:
	makeobj c static\AUDIODCT.KDR kdradict.obj _audiodict

kdrahead.obj:
	makeobj f static\AUDIOHHD.KDR kdrahead.obj _AudioHeader _audiohead

kdrcdict.obj:
	makeobj c static\CGADICT.KDR kdrcdict.obj _CGAdict

kdrchead.obj:
	makeobj f static\CGAHEAD.KDR kdrchead.obj CGA_grafixheader _CGAhead

context.obj:
	makeobj f static\CONTEXT.KDR context.obj

kdredict.obj:
	makeobj c static\EGADICT.KDR kdredict.obj _EGAdict

kdrehead.obj:
	makeobj f static\EGAHEAD.KDR kdrehead.obj EGA_grafixheader _EGAhead

gametext.obj:
	makeobj f static\GAMETEXT.KDR gametext.obj

kdrmdict.obj:
	makeobj c static\MAPDICT.KDR kdrmdict.obj _mapdict

kdrmhead.obj:
	makeobj f static\MAPHEAD.KDR kdrmhead.obj MapHeader _maphead

piracy.h:
	makeobj s static\PIRACY.SCN piracy.h 7

story.obj:
	makeobj f static\STORY.KDR story.obj
